var express = require("express");
var router = express.Router();

// Load User model
const Applicant = require("../models/Applicants");
const Recruiter = require('../models/Recruiters');
// GET request 
// Getting all the users
router.get("/", function(req, res) {
    Applicant.find(function(err, users) {
		if (err) {
			console.log(err);
		} else {
			res.json(users);
		}
	})
});

// NOTE: Below functions are just sample to show you API endpoints working, for the assignment you may need to edit them

// POST request 
// Add a user to db
router.post("/registerApplicant", (req, res) => {
    const newUser = new Applicant({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
        userType: req.body.userType,
        fname: req.body.fname,
        lname: req.body.lname,
        education: req.body.education,
        skills: req.body.skills,
        date: req.body.date,
        rating: 0,
        reviews: 0
    });
    Applicant.findOne({$or:[{email:req.body.email,userType:req.body.userType}, {name:req.body.name,userType:req.body.userType}]}).then((user)=>{
        if(!user){
            newUser.save()
            .then(user => {
                res.status(200).json(user);
                console.log('new user '+ user);
            })
            .catch(err => {
                console.log(err);
                res.status(400).send(err);
            });
        }else{
            res.status(400).send("Email/Username already used");
        }
    });
});
router.post("/registerRecruiter", (req, res) => {
    const newUser = new Recruiter({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
        userType: req.body.userType,
        bio: req.body.bio,
        fname: req.body.fname,
        lname: req.body.lname,
        number: req.body.number,
        date: req.body.date
    });
    Recruiter.findOne({$or:[{email:req.body.email,userType:req.body.userType}, {name:req.body.name,userType:req.body.userType}]}).then((user)=>{
        if(!user){
            newUser.save()
            .then(user => {
                res.status(200).json(user);
                console.log('new user '+ user);
            })
            .catch(err => {
                console.log(err);
                res.status(400).send(err);
            });
        }else{
            res.status(400).send("Email/Username already used");
        }
    });
});
// POST request 
// Login
router.post("/login", (req, res) => {
    const email = req.body.email;
    const password = req.body.password;
    const userType = req.body.userType;
    // Find user by email
    if(userType === 'Job Applicant'){
        Applicant.findOne({ "email":email, "password":password, "userType":userType }).then(user => {
            // Check if user email exists
            if (!user) {
                return res.json({msg:'error'});
            }
            else{
                return res.json(user);
            }
        });
    }
    else{
        Recruiter.findOne({ "email":email, "password":password, "userType":userType }).then(user => {
            // Check if user email exists
            if (!user) {
                return res.json({msg:'error'});
            }
            else{
                return res.json(user);
            }
        });
    }
	
});

// POST request
// finding applicant based on email
router.post('/findApplicant', (req, res) => {
        Applicant.findOne({"email":req.body.email})
        .then(user => {
            if(!user) res.json({msg:'error'});
            else res.json(user);
        })
        .catch(err => console.log(err));
});
router.post('/findRecruiter', (req, res) => {
    Recruiter.findOne({"email":req.body.email})
    .then(user => {
        if(!user) res.status(400);
        else res.json(user);
    })
    .catch(err => console.log(err));
});
// post request
// editing profile
router.post('/updateApplicant', (req, res) => {
    Applicant.findOne({email:req.body.email}).then(user => {
        if(user.name === req.body.name){
            Applicant.findOneAndUpdate({email:req.body.email},{$set:{
                password: req.body.password,
                userType: req.body.userType,
                fname: req.body.fname,
                lname: req.body.lname,
                education: req.body.education,
                skills: req.body.skills,
            }}).then(user => res.json(user)).catch(err => console.log(err))
        }else{
            Applicant.findOne({name:req.body.name})
                .then(user => {
                    if(!user){
                        Applicant.findOneAndUpdate({email:req.body.email},{$set:{
                            name: req.body.name,
                            email: req.body.email,
                            password: req.body.password,
                            userType: req.body.userType,
                            fname: req.body.fname,
                            lname: req.body.lname,
                            education: req.body.education,
                            skills: req.body.skills,
                        }}).then(user => res.json(user))
                    }else{
                        res.status(400).send('User name already used')
                    }
                }
            )
        }
    })
})



router.post('/updateRecruiter', (req, res) => {
    Recruiter.findOne({email:req.body.email}).then(user => {
        if(user.name === req.body.name){
            Recruiter.findOneAndUpdate({email:req.body.email},{$set:{
                password: req.body.password,
                userType: req.body.userType,
                bio: req.body.bio,
                fname: req.body.fname,
                lname: req.body.lname,
                number: req.body.number
            }}).then(user => res.json(user)).catch(err => console.log(err))
        }else{
            Recruiter.findOne({name:req.body.name})
                .then(user => {
                    if(!user){
                        Recruiter.findOneAndUpdate({email:req.body.email},{$set:{
                            name: req.body.name,
                            email: req.body.email,
                            password: req.body.password,
                            userType: req.body.userType,
                            bio: req.body.bio,
                            fname: req.body.fname,
                            lname: req.body.lname,
                            number: req.body.number
                        }}).then(user => res.json(user))
                    }else{
                        res.status(400).send('User name already used')
                    }
                }
            )
        }
    })
})

// reviewing a applicant
router.post('/review',(req,res) => {
    Applicant.findOneAndUpdate ({email:req.body.email},{$inc:{rating:req.body.rating, reviews:1}})
        .then(applicant => {
            res.json(applicant);
        })
        .catch(err => console.log(err));
})
module.exports = router;
var express = require("express");
var router = express.Router();

const Application = require("../models/Applications");
const Job = require("../models/Jobs");
// add new application
router.post("/add", (req, res) => {
    const newApplication = new Application({
        email_applicant: req.body.email_applicant,
        email_recruiter: req.body.email_recruiter,
        title: req.body.title,
        sop : req.body.sop,
        doj : 'Didnot join',
        doa: req.body.doa,
        status: 'Applied',
        name_recruiter: req.body.name_recruiter,
        salary: req.body.salary,
        review: 'not yet done',
        review_applicant: 'not yet done'
    });
    newApplication.save()
        .then(x => res.status(200).json({msg:'ok'}))
        .catch(err => console.log(err));
})

// to make sure incase applicant had already applied
router.post("/search", (req,res) => {
    Application.findOne({email_applicant: req.body.email_applicant, email_recruiter: req.body.email_recruiter, title: req.body.title})
        .then(application => {
            if(!application){
                res.status(200).json({msg:'Apply'});
            }else{
                res.status(200).json({msg: 'Applied'});
            }
        })
        .catch(err => console.log(err));
})

router.post("/myapplications", (req,res) => {
    Application.find({email_applicant: req.body.email})
        .then(applications => res.status(200).json(applications))
        .catch(err => console.log(err));
})

router.post("/changes", (req,res) => {
    Job.findOne({email:req.body.email_recruiter,title:req.body.title})
        .then(job => {
            Job.findOneAndUpdate({email:req.body.email_recruiter,title:req.body.title},{$set:{applied:String(Number(job.applied)+1)}}).then(r => console.log('ok')).catch(err => console.log(err));
            res.status(200).json({msg:'ok'});
        })
        .catch(err => console.log(err));
})

// to change status of the application

// shortlisted
router.post("/updates", (req,res) => {
    Application.findOneAndUpdate({email_applicant: req.body.email_applicant, title: req.body.title, email_recruiter:req.body.email_recruiter},{$set:{status: 'ShortListed'}})
        .then(job => res.status(200).json({msg:'ok'}))
        .catch(err => console.log(err));
})
// accepted
router.post("/updatea", (req,res) => {
    Application.findOneAndUpdate({email_applicant: req.body.email_applicant, title: req.body.title, email_recruiter: req.body.email_recruiter},{$set:{status: 'Accepted',doj:req.body.doj}})
        .then(job => console.log('ok'))
        .catch(err => console.log(err));
    Job.findOne({email:req.body.email_recruiter, title:req.body.title})
        .then(job => {
            Job.findOneAndUpdate({email:req.body.email_recruiter, title:req.body.title},{$set:{filled:String(Number(job.filled)+1)}})
                .then(job => res.json({filled:String(Number(job.filled)+1),positions:job.positions}))
                .catch(err => console.log(err));
        })
        .catch(err => console.log(err));
})
// rejected
router.post("/updater", (req,res) => {
    Application.findOneAndUpdate({email_applicant: req.body.email_applicant, title: req.body.title, email_recruiter:req.body.email_recruiter},{$set:{status: 'Rejected'}})
        .then(job => res.status(200).json({msg:'ok'}))
        .catch(err => console.log(err));
})

// reject remaining all after all the jobs get filled

router.post("/rejectrem",(req,res) => {
    Application.updateMany({email_recruiter:req.body.email_recruiter, title:req.body.title,status:{$ne: 'Accepted'}},{$set:{status:'Rejected'}})
        .then(res => console.log('ok'))
        .catch(err => console.log(err));
})

//  all applications related to one particular job

router.post("/applications", (req,res) => {
    Application.find({email_recruiter:req.body.email, title: req.body.title})
        .then(applications => res.status(200).json(applications))
        .catch(err => console.log(err));
})

// all accepted applications under a particular recruiter

router.post("/acceptedapplications", (req,res) => {
    Application.find({email_recruiter: req.body.email, status: 'Accepted'})
        .then(applications => res.status(200).json(applications))
        .catch(err => console.log(err));
})

// updating review
router.post("/updatereview", (req,res) => {
    Application.findOneAndUpdate({title: req.body.title,email_recruiter: req.body.email_recruiter, email_applicant: req.body.email_applicant},{$set:{review:'done'}})
            .then(application => res.json(application))
            .catch(err => console.log(err));
})

router.post("/updatereview_applicant", (req,res) => {
    Application.findOneAndUpdate({title: req.body.title,email_recruiter: req.body.email_recruiter, email_applicant: req.body.email_applicant},{$set:{review_applicant:'done'}})
            .then(application => res.json(application))
            .catch(err => console.log(err));
})


//deleting applications corresponding to a particular job
router.post("/deletejob",(req,res)=>{
    console.log(req.body);
    Application.deleteMany({email_recruiter:req.body.email_recruiter,title: req.body.title})
        .then(val => res.json('done'))
        .catch(err => console.log(err));
})
module.exports = router;
var express = require("express");
var router = express.Router();

const Job = require("../models/Jobs");

// save job details
router.post("/", (req,res) => {
    const newJob = new Job({
        title: req.body.title,
        name: req.body.name,
        email: req.body.email,
        applications: req.body.max_app,
        positions: req.body.avl_pos,
        salary: req.body.salary,
        jobtype: req.body.JobType,
        duration: req.body.duration,
        deadline: req.body.deadline,
        date: req.body.date,
        applied: req.body.applied,
        skills: req.body.skills,
        filled: req.body.filled,
        status: req.body.status,
        rating: 0,
        reviews: 0
    });
    Job.findOne({"title":req.body.title,"email":req.body.email}).then(job => {
        if(!job){
            newJob.save()
            .then(job => res.status(200).json({msg:'ok'}))
            .catch(err => {
                console.log(err);
                res.status(400);
            });
        }
        else{
            res.status(200).json({msg:"there is an active job with the same title"})
        }
    });
})

// retrive a job with given title

router.post("/getjob", (req, res) => {
    Job.findOne({email: req.body.email,title:req.body.title})
        .then(job => res.status(200).json(job))
        .catch(err => console.log(err));
})

// View all jobs posted by a particular recruiter

router.post("/viewjobs", (req,res) => {
    Job.find({email:req.body.email}).then(jobs => {
        res.status(200).json(jobs);
    })
    .catch(err => console.log(err));
});

// edit job

router.post("/editjob", (req, res) => {
    Job.findOneAndUpdate({email:req.body.email, title:req.body.title},{$set:{
        applications: req.body.applications,
        positions: req.body.positions,
        deadline : req.body.deadline
    }}).then(job => res.status(200).json({msg:'ok'})).catch(err => res.status(400) );
})

// search jobs

router.post("/searchjobs",(req,res) => {
    Job.find().then(jobs => res.status(200).json(jobs)).catch(err => console.log(err));
})

// applicant reviews the job 
router.post("/review",(req,res) => {
    Job.findOneAndUpdate({title: req.body.title, email: req.body.email},{$inc:{rating:Number(req.body.rating), reviews: 1}})
        .then(job => res.json(job))
        .catch(err => console.log(err));
})
// deleting a job
router.post("/deletejob", (req,res)=>{
    console.log(req.body);
    Job.findOneAndDelete({email:req.body.email,title:req.body.title})
        .then(res => console.log('okee'))
        .catch(err => consolelog(err));
})

// making job as inactive
router.post("/statusinactive",(req,res) => {
    Job.findOneAndUpdate({title:req.body.title, email: req.body.email},{$set:{status: 'inactive'}})
        .then(job => res.json('ok'))
        .catch(err => console.log(err));
})

// making job as active
router.post("/statusactive",(req,res) => {
    Job.findOneAndUpdate({title:req.body.title, email: req.body.email},{$set:{status: 'active'}})
        .then(job => res.json('ok'))
        .catch(err => console.log(err));
})
module.exports = router;
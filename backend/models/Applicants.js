const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const ApplicantSchema = new Schema({
	name: {
		type: String,
		required: true
	},
	email: {
		type: String,
		required: true
	},
	password: {
		type: String,
		required: true
	},
	userType: {
		type: String,
		required: true
	},
	fname: {
		type: String,
		required: true
	},
	lname: {
		type: String,
		required: true
	},
	education: {
		type: Array,
		required: true
	},
	skills: {
		type: Array,
		required: true
	},
	date:{
		type: Date,
		required: false
	},
	reviews:{
		type: Number,
		required: true
	},
	rating: {
		type: Number,
		required: true
	}
});

module.exports = Applicant = mongoose.model("Applicant", ApplicantSchema);

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ApplicationSchema = new Schema({
    email_applicant: {
        type: String,
        required: true
    },
    email_recruiter: {
        type: String,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    sop :{
        type: String,
        required: true
    },
    doj :{
        type: String,
        required: true
    },
    doa :{
        type: String,
        required: true
    },
    status :{
        type: String,
        required: true
    },
    salary: {
        type: String,
        required: true
    },
    name_recruiter:{
        type:String,
        required: true
    },
	review: {
		type: String,
		required: true
	},
	review_applicant: {
		type: String,
		required: true
	}
});

module.exports = Application = mongoose.model("Application", ApplicationSchema);
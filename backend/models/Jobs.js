const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const JobSchema = new Schema({
	title: {
		type: String,
		required: true
    },
    name: {
		type: String,
		required: true
	},
	email: {
		type: String,
		required: true
	},
	applications: {
		type: String,
		required: true
	},
	positions: {
		type: String,
		required: true
	},
	salary: {
		type: String,
		required: true
	},
	deadline: {
		type: String,
		required: true
	},
	jobtype: {
		type: String,
		required: true
	},
	skills: {
		type: Array,
		required: true
    },
    duration: {
        type: String,
        required: true
    },
    applied: {
        type: String,
        required: true
	},
	filled: {
        type: String,
        required: true
    },
    status: {
        type: String,
        required: true
    },
	date:{
		type: String,
		required: true
	},
	rating: {
		type: Number,
		required: true
	},
	reviews: {
		type: Number,
		required: true
	}
});

module.exports = Job = mongoose.model("Job", JobSchema);

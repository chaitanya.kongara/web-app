const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const RecruiterSchema = new Schema({
	name: {
		type: String,
		required: true
	},
	email: {
		type: String,
		required: true
	},
	password: {
		type: String,
		required: true
	},
	userType: {
		type: String,
		required: true
	},
	fname: {
		type: String,
		required: true
	},
	lname: {
		type: String,
		required: true
	},
	number: {
        type: String,
        required: true
    },
    bio: {
        type: String,
        required: true
    },
	date:{
		type: Date,
		required: false
	}
});

module.exports = Recruiter = mongoose.model("Recruiter", RecruiterSchema);

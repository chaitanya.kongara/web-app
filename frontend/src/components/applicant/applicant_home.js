import React, {Component} from 'react';
import axios from 'axios';
import "bootstrap/dist/css/bootstrap.min.css";
import NavBara from '../templates/NavbarApplicant';
import { responsiveFontSizes } from '@material-ui/core';
export default class ApplicantHome extends Component{
    constructor(props){
        super(props);
    }
    render() {
        return (
            <div>
                <NavBara/>
                <br/>
                <h1>WELCOME {localStorage.getItem("name")}(Applicant)</h1>
            </div>
        )
    }
}
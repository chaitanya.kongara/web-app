import React, {Component} from 'react';
import axios from 'axios';
import "bootstrap/dist/css/bootstrap.min.css";
import NavBara from '../templates/NavbarApplicant';
var myapplications = [];
var accepted = false;
export default class SearchJob extends Component{
    constructor(props){
        super(props);
        this.state = {
            email: localStorage.getItem('id'),
            search: '',
            radio: 'None',
            show: false,
            jobs: [],
            FT: false,
            PT: false,
            WFH: false,
            min_salary: 0,
            max_salary: 10000000000,
            duration: 7,
            sop: 'this is sop',
            open: false
        }
    }
    componentDidMount(){
        axios.post("http://localhost:4000/newapplication/myapplications",{email:localStorage.getItem('id')})
            .then(res => {
                myapplications = res.data
                myapplications.map((ele,ind)=>{
                    if(myapplications[ind].status === 'Accepted') accepted = true;
                })
            })
            .catch(err => console.log(err));
    }
    onChangeSearch(event){
        this.setState({search: event.target.value});
    }
    OnChangeRadio(event){
        this.setState({radio: event.target.value});
    }
    onChangeFT(event){
        this.setState({FT:(this.state.FT ? false : true)});
    }
    onChangePT(event){
        this.setState({PT:(this.state.PT ? false : true)});
    }
    onChangeWFH(event){
        this.setState({WFH:(this.state.WFH ? false : true)});
    }
    onChangeMinSalary(event){
        this.setState({min_salary: event.target.value});
    }
    onChangeMaxSalary(event){
        console.log(isNaN(event.target.value));
        this.setState({max_salary: event.target.value});
    }
    onChangeDuration(event){
        this.setState({duration : event.target.value});
    }
    onSubmit(e){
        e.preventDefault();
        if(this.state.max_salary < this.state.min_salary) alert("Max Salary < Min Salary!");
        else{axios.post('http://localhost:4000/createjob/searchjobs', {})
            .then(res => {
                this.setState({jobs:[]})
                res.data.map((ele,idx) => {
                    var today = new Date()
                    var here = today.getFullYear() + '-' + String(today.getMonth() + 1).padStart(2, '0')  + '-' + String(today.getDate()).padStart(2, '0') +'T'+String(today.getHours()).padStart(2, '0') + ':' + String(today.getMinutes()).padStart(2, '0');
                    console.log(here+'jj');
                    if(res.data[idx].title.toLowerCase().search(this.state.search.toLowerCase()) == 0 && Number(res.data[idx].salary)<=this.state.max_salary && Number(res.data[idx].salary)>=this.state.min_salary && ((res.data[idx].jobtype === 'Full Time' && this.state.FT) || (res.data[idx].jobtype === 'Part Time' && this.state.PT) || (res.data[idx].jobtype === 'Work From Home' && this.state.WFH)) && Number(res.data[idx].duration) < Number(this.state.duration) && res.data[idx].deadline > here){
                        var temp = [...this.state.jobs,res.data[idx]];
                        var rating =  (res.data[idx].reviews ? (res.data[idx].rating / res.data[idx].reviews) : 0)
                        temp[temp.length-1].rating = rating;
                        this.setState({jobs:temp})
                    }
                })
                if(this.state.radio === 'sa'){
                    var temp;
                    temp = this.state.jobs.sort((a,b) => {return Number(a.salary) - Number(b.salary)});
                    this.setState({jobs: temp});
                }
                else if(this.state.radio === 'sd'){
                    var temp;
                    temp = this.state.jobs.sort((a,b) => {return Number(b.salary) - Number(a.salary)});
                    this.setState({jobs: temp});
                }
                else if(this.state.radio === 'da'){
                    var temp;
                    temp = this.state.jobs.sort((a,b) => {return Number(a.duration) - Number(b.duration)});
                    this.setState({jobs: temp});
                }
                else if(this.state.radio === 'dd'){
                    var temp;
                    temp = this.state.jobs.sort((a,b) => {return Number(b.duration) - Number(a.duration)});
                    this.setState({jobs: temp});
                }
                else if(this.state.radio === 'ra'){
                    var temp;
                    temp = this.state.jobs.sort((a,b) => {return Number(a.rating) - Number(b.rating)});
                    this.setState({jobs: temp});
                }
                else if(this.state.radio === 'rd'){
                    var temp;
                    temp = this.state.jobs.sort((a,b) => {return Number(b.rating) - Number(a.rating)});
                    this.setState({jobs: temp});
                }
                this.setState({show: true})
            })
            .catch(err => console.log(err));
        }
    }
    onApply(idx,e){
        e.preventDefault();
        if(myapplications.length === 10) alert("Reached ur max limit of 10 open applications :/");
        else{
            var SOP = prompt('Please enter SOP');
            if(SOP != null){
            if(SOP != ''){ 
            var cnt=0;
            var val = SOP;
            SOP = SOP.trim();
            SOP = SOP.split(' ');
            SOP.map((ele,idx) => {
                if(ele !== '') cnt++;
            });
            if(cnt>250) alert('Oops! At max 250 words');
            else{
            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); 
            var yyyy = today.getFullYear();
            this.state.doa = dd + "/" + mm + "/" + yyyy;
            const obj = {
            email_applicant: localStorage.getItem('id'),
            email_recruiter: this.state.jobs[idx].email,
            title: this.state.jobs[idx].title,
            sop: val,
            salary: this.state.jobs[idx].salary,
            name_recruiter: this.state.jobs[idx].name,
            doa: this.state.doa
            };
            var temp = [...myapplications,obj];
            myapplications = temp;
            axios.post("http://localhost:4000/newapplication/add", obj).then(res => {
                console.log('ok');
                alert('Applied!');
                axios.post("http://localhost:4000/newapplication/changes", obj).then(res => {
                    console.log('ok..');
                    this.props.history.push('/Applicant-home');
                })
                .catch(err => console.log(err));
            })
            .catch(err => console.log(err));}}}}
    }
    render(){
        return (
            <div>
                <NavBara/>
                <br/>
                <form>
                    <div className="form-group">
                        <label><h2>Search:</h2></label>
                        <input type="string" 
                               className="form-control" 
                               value={this.state.search}
                               onChange={this.onChangeSearch.bind(this)}
                               required
                               placeholder="Job title"
                               />
                    </div>
                    <div>
                        <label><h2>Sort based on: (ASC: ascending, DESC: descending)</h2></label>
                    </div>
                    <div onChange={this.OnChangeRadio.bind(this)}>
                        <input type="radio" value="sa" checked={this.state.radio === 'sa'}/>Salary ASC
                        <br/>
                        <br/>
                        <input type="radio" value="sd" checked={this.state.radio === 'sd'}/>Salary DESC
                        <br/>
                        <br/>
                        <input type="radio" value="da" checked={this.state.radio === 'da'}/>Duration ASC
                        <br/>
                        <br/>
                        <input type="radio" value="dd" checked={this.state.radio === 'dd'}/>Duration DESC
                        <br/>
                        <br/>
                        <input type="radio" value="ra" checked={this.state.radio === 'ra'}/>Rating ASC
                        <br />
                        <br/>
                        <input type="radio" value="rd" checked={this.state.radio === 'rd'}/>Rating DESC
                        <br />
                        <br/>
                        <input type="radio" value="None" checked={this.state.radio === 'None'}/>None
                    </div>
                    <br/>
                    <div>
                        <label><h2>Filters:</h2></label>
                    </div>
                    <div>
                        <lablel><h3>Check the JobTypes you are interested in :</h3></lablel>
                    </div>
                    <div>
                        <input type="checkbox" value="FT" checked={this.state.FT} onChange={this.onChangeFT.bind(this)}/>
                        <label>Full Time</label>
                    </div>
                    <div>
                        <input type="checkbox" value="PT" checked={this.state.PT} onChange={this.onChangePT.bind(this)}/>
                        <label>Part Time</label>
                    </div>
                    <div>
                        <input type="checkbox" value="WFH" checked={this.state.WFH} onChange={this.onChangeWFH.bind(this)}/>
                        <label>Work From Home</label>
                    </div>
                    <div>
                        <label><h3>mention the salary range</h3></label><br/>
                        <input type="number" min="0" max={this.state.max_salary} placeholder="Min Salary"  value={this.state.min_salary} onChange={this.onChangeMinSalary.bind(this)} required/>
                        <input type="number" min={this.state.min_salary} max="10000000000" placeholder="Max Salary" value={this.state.max_salary} onChange={this.onChangeMaxSalary.bind(this)} required/>
                    </div>
                    <br/>
                    <div>
                    <label>Duration less than the chosen value: </label>
                        <select value={this.state.duration} onChange={this.onChangeDuration.bind(this)} required>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                        </select>
                    </div>
                    <br/>
                    <div className="form-group">
                        <input type="submit" value="search" className="btn btn-primary" onClick={this.onSubmit.bind(this)}/>
                    </div>
                    {
                        this.state.show === true && <div>
                            <table className="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Recruiter Name</th>
                                        <th>Rating</th>
                                        <th>Salary</th>
                                        <th>Duration</th>
                                        <th>Deadline</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                   {
                                       this.state.jobs.map((ele, idx) => {
                                        var full=false;
                                        var applied=false;
                                        var rejected=false;
                                           if(this.state.jobs[idx].applied === this.state.jobs[idx].applications || this.state.jobs[idx].filled === this.state.jobs[idx].positions) full = true;
                                           else full = false;
                                           applied = false;
                                            myapplications.map((ele, ind) => {
                                                if(this.state.jobs[idx].email === myapplications[ind].email_recruiter && this.state.jobs[idx].title === myapplications[ind].title){
                                                    if(myapplications[ind].status === 'Applied' || myapplications[ind].status === 'ShortListed' || myapplications[ind].status==='Accepted') applied = true;
                                                    else if(myapplications[ind].status === 'Rejected') rejected = true;
                                                }
                                            })
                                            return (
                                                <tr>
                                                    <td>{this.state.jobs[idx].title}</td>
                                                    <td>{this.state.jobs[idx].name}</td>
                                                    <td>{this.state.jobs[idx].rating} </td>
                                                    <td>{this.state.jobs[idx].salary}</td>
                                                    <td>{this.state.jobs[idx].duration}</td>
                                                    <td>{this.state.jobs[idx].deadline}</td>
                                                    <td>{accepted === true && <button type="button" class="btn btn-info" >Can't Apply</button>}
                                                    {rejected&&!accepted === true && <button type="button" class="btn btn-danger" >Rejected</button>}
                                                    {applied&&!rejected&&!accepted === true && <button type="button" class="btn btn-warning" >Applied</button>}
                                                    {(full&&!applied&&!rejected&&!accepted)  === true && <button type="button" class="btn btn-danger">Full</button>}
                                                    {(!full&&!applied&&!rejected&&!accepted) === true && <button type="button" class="btn btn-primary" onClick={this.onApply.bind(this,idx)}>Apply</button>}</td>                                                    
                                                </tr>
                                            )
                                        })
                                   }
                                </tbody>
                            </table>
                        </div>
                    }
                </form>
            </div>
        )
    }
}
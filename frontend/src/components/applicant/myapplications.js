import React, {Component} from 'react';
import axios from 'axios';
import "bootstrap/dist/css/bootstrap.min.css";
import NavBara from '../templates/NavbarApplicant';

export default class MyApplications extends Component{
    constructor(props){
        super(props);
        this.state = {applications: []}
    }
    componentDidMount(){
        axios.post("http://localhost:4000/newapplication/myapplications", {email:localStorage.getItem('id')})
            .then(res => {
                this.setState({applications: res.data});
            })
            .catch(err => console.log(err));
    }
    onReview(idx,e){
        e.preventDefault();
        var review = prompt('rating(on scale 0-5):');
        if(review != null){
            if(review == '' || review.length != 1 || (review[0] < '0' || review[0] > '5')) alert('enter on scale of 0-5, try again');
            else{
                axios.post("http://localhost:4000/createjob/review",{email: this.state.applications[idx].email_recruiter , rating: review[0], title:this.state.applications[idx].title})
                    .then( res => {
                        alert('rating sent')
                        axios.post("http://localhost:4000/newapplication/updatereview_applicant",{title:this.state.applications[idx].title,email_applicant:this.state.applications[idx].email_applicant, email_recruiter:this.state.applications[idx].email_recruiter})
                            .then(res => {
                                var temp = [...this.state.applications];
                                temp[idx].review_applicant = 'done';
                                this.setState({applications: temp});
                                console.log('processing');
                            })
                            .catch(err => console.log(err));
                    })
                    .catch(err => console.log(err));
            }
        }
    }
    render(){
        return (
            <div>
                <NavBara/>
                <br/>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Job Title</th>
                            <th>Date of Joining</th>
                            <th>Salary</th>
                            <th>Recruiter's Name</th>
                            <th>Status</th>
                            <th>rating option</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.applications.map((ele, idx) => {
                                return (
                                    <tr>
                                        <td>{this.state.applications[idx].title}</td>
                                        <td>{this.state.applications[idx].doj }</td>
                                        <td>{this.state.applications[idx].salary}</td>
                                        <td>{this.state.applications[idx].name_recruiter}</td>
                                        <td>{this.state.applications[idx].status}</td>
                                        <td>{this.state.applications[idx].status === 'Accepted' && this.state.applications[idx].review_applicant === 'not yet done' &&  <button type="button" class="btn btn-primary" onClick={this.onReview.bind(this,idx)}>Review</button> }
                                        {this.state.applications[idx].review_applicant === 'done' && <a>Done</a>}</td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>
        )
    }
}
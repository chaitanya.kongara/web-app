import React, {Component} from 'react';
import axios from 'axios';
import "bootstrap/dist/css/bootstrap.min.css"
var cnt = 0;
export default class Register extends Component {
    
    constructor(props) {
        console.log('new');
        super(props);

        this.state = {
            email: '',
            password: '',
            userType: '',
            date:null
        }

        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
        this.onChangeuserType = this.onChangeuserType.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onChangeEmail(event) {
        this.setState({ email: event.target.value });
    }
    onChangePassword(event) {
        this.setState({ password: event.target.value });
    }
    onChangeuserType(event) {
        this.setState({ userType: event.target.value });
    }  

    onSubmit(e) {
        e.preventDefault();
        if(this.state.userType === 'SELECT'){
            alert("Oops! Forgot to choose user-type");
        }else{
            if(this.state.userType === 'Job Applicant'){
                const newUser = {
                    email: this.state.email,
                    password: this.state.password,
                    userType: this.state.userType,
                    date: Date.now()
                }
                console.log(newUser);
                axios.post('http://localhost:4000/user/login', newUser)
                    .then(res => {
                        alert("Login successful!");
                        console.log(res.data);
                        this.props.history.push('/Applicant-Home');
                    })
                    .catch(err => {alert("Oops! Invalid credentials");})
                    ;
            }
            else{
                const newUser = {
                    email: this.state.email,
                    password: this.state.password,
                    userType: this.state.userType,
                    date: Date.now()
                }
                axios.post('http://localhost:4000/user/login', newUser)
                    .then(res => {
                        alert("Login successful");
                        console.log(res.data);
                        this.props.history.push('/Recruiter-Home');
                    })
                    .catch(err => {alert("Oops! Invalid credentials");})
                    ;
            }
        }
    }
    render() {
        return (
            <div>
                <form >
                    <div className="form-group">
                        <label>Email: </label>
                        <input type="email" 
                               className="form-control" 
                               value={this.state.email}
                               onChange={this.onChangeEmail}
                               required
                               />  
                    </div>
                    <div className="form-group">
                        <label>Password: </label>
                        <input type="password" 
                               className="form-control" 
                               value={this.state.password}
                               onChange={this.onChangePassword}
                               required
                               />  
                    </div>
                    <div className="form-group">
                        <label>User Type: </label>
                        <br/>
                        <select  value={this.state.userType} onChange={this.onChangeuserType}>
                            <option value="SELECT">--SELECT--</option>
                            <option value="Job Applicant">Job Applicant</option>
                            <option value="Job Recruiter">Job Recruiter</option>
                        </select>
                    </div>
                    <br/>
                    {console.log(this.state)}
                    <div className="form-group">
                        <input type="submit" value="Login" className="btn btn-primary" onClick={this.onSubmit}/>
                    </div>
                </form>
            </div>
        )
    }
}
import React, {Component} from 'react';
import axios from 'axios';
import "bootstrap/dist/css/bootstrap.min.css";
import NavBarr from '../templates/NavbarRecruiter';
import { responsiveFontSizes } from '@material-ui/core';
export default class RecruiterHome extends Component{
    constructor(props){
        super(props);
    }
    render() {
        return (
            <div>
                <NavBarr/>
                <br/>
                <h1>WELCOME {localStorage.getItem("name")}(Recruiter)</h1>
            </div>
        )
    }
}
import React, {Component} from 'react';
import axios from 'axios';
import "bootstrap/dist/css/bootstrap.min.css";
import NavBarr from '../templates/NavbarRecruiter';

var applicants = [];
var sorting_obj ={
    email: [],
    applications: []
}
export default class JobApplications extends Component{
    constructor(props){
        super(props);
        this.state = {
            title: localStorage.getItem('title'),
            email: localStorage.getItem('id')
        }
    }
    componentDidMount(){
        axios.post('http://localhost:4000/newapplication/applications',this.state)
            .then(res => {
                sorting_obj.applications = [...res.data];
                console.log(sorting_obj.applications, res.data);
                this.setState(this.state, () => {
                    axios.get('http://localhost:4000/user/')
                    .then(res => {
                        var temp = [...res.data];
                        applicants = temp;
                        console.log(applicants,res.data);
                        this.setState(this.state);
                    })
                    .catch(err => console.log(err));
                });
            })
            .catch(err => console.log(err));
    }
    onShortList(idx,e){
        e.preventDefault();
        sorting_obj.applications[idx].status = 'ShortListed';
        axios.post('http://localhost:4000/newapplication/updates',{email_recruiter:this.state.email ,title:this.state.title,email_applicant:sorting_obj.applications[idx].email_applicant})
            .then(res => this.props.history.push('/JobApplications'))
            .catch(err => console.log(err));
            this.setState(this.state);
            this.props.history.push('/Recruiter-Home')
    }
    onAccept(idx,e){
        sorting_obj.applications[idx].status = 'Accepted';
        e.preventDefault();
        var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); 
            var yyyy = today.getFullYear();
            today = dd + "/" + mm + "/" + yyyy;
        axios.post('http://localhost:4000/newapplication/updatea',{email_recruiter:this.state.email ,title:this.state.title,email_applicant:sorting_obj.applications[idx].email_applicant,doj:today})
            .then(res => {
                if(res.data.filled == res.data.positions){
                axios.post('http://localhost:4000/newapplication/rejectrem',{email_recruiter:this.state.email ,title:this.state.title})
                    .then(res => {
                        console.log(res.body);
                        this.props.history.push('/myjobs')
                    })
                    .catch(err => console.log(err));
                axios.post("http://localhost:4000/createjob/statusinactive",{title:localStorage.getItem('title'),email:localStorage.getItem('id')})
                    .then(res => {
                        console.log(res.body);
                        this.props.history.push('/myjobs')
                    })
                    .catch(err => console.log(err));
                }
            })
            .catch(err => console.log(err));
            this.setState(this.state);
            this.props.history.push('/myjobs')
    }
    onReject(idx,e){
        sorting_obj.applications[idx].status = 'Rejected';
        e.preventDefault();
        axios.post('http://localhost:4000/newapplication/updater',{email_recruiter:this.state.email ,title:this.state.title,email_applicant:sorting_obj.applications[idx].email_applicant})
            .then(res => this.props.history.push('/JobApplications'))
            .catch(err => console.log(err));
            this.setState(this.state);
            this.props.history.push('/Recruiter-Home')
    }
    // Name, Skills, Date of Application, Education, SOP, Rating, Stage of Application in view
    render(){
        return (
            <div>
                <NavBarr/>
                <br/>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Skills</th>
                            <th>Date of Application</th>
                            <th>Education</th>
                            <th>SOP</th>
                            <th>Rating</th>
                            <th>status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            sorting_obj.applications.map((ele,idx) => {
                                var temp1 = '';
                                var temp2 ='';
                                var name = '';
                                var rating;
                                if(sorting_obj.applications[idx].status != 'Rejected'){
                                applicants.map((ele, ind) => {
                                    if(applicants[ind].email === sorting_obj.applications[idx].email_applicant ){
                                        name = applicants[ind].name;
                                        if(applicants[ind].reviews == 0) rating='NA';
                                        else rating = String(applicants[ind].rating/applicants[ind].reviews);
                                        sorting_obj.email = [...sorting_obj.email,applicants[ind].email];
                                        for(let i=0;i<applicants[ind].skills.length;i++){
                                            if(i == (applicants[ind].skills.length-1)){
                                                 temp1 = temp1 + applicants[ind].skills[i];
                                                 console.log(temp1);
                                            }
                                            else{
                                                temp1 = temp1 + applicants[ind].skills[i] + ', ';
                                            }
                                        }
                                        for(let i =0; i<applicants[ind].education.length ;i++){
                                            if(i == (applicants[ind].education.length-1)){
                                                temp2 = temp2 + applicants[ind].education[i].Iname;
                                                console.log(temp2);
                                            }
                                            else{
                                                temp2 = temp2 + applicants[ind].education[i].Iname + ', ';
                                            }
                                        }
                                    }
                                })
                                return (
                                    <tr>
                                        <td>{name}</td>
                                        <td>{temp1}</td>
                                        <td>{sorting_obj.applications[idx].doa}</td>
                                        <td>{temp2}</td>
                                        <td>{sorting_obj.applications[idx].sop}</td>
                                        <td>{rating}</td>
                                        <td>{sorting_obj.applications[idx].status}</td>
                                        <td>{sorting_obj.applications[idx].status === 'Applied' && <button type="button" class="btn btn-primary" onClick={this.onShortList.bind(this,idx)}>ShortList</button>}
                                        {sorting_obj.applications[idx].status === 'ShortListed' && <button type="button" class="btn btn-success" onClick={this.onAccept.bind(this,idx)}>Accept</button>}&nbsp;{ sorting_obj.applications[idx].status!== 'Accepted' && <button type="button" class="btn btn-danger" onClick={this.onReject.bind(this,idx)} >Reject</button>}</td>
                                    </tr>
                                )}
                            })
                        }
                    </tbody>
                </table>
            </div>
        )
    }
}
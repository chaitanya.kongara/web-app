import React, {Component} from 'react';
import axios from 'axios';
import "bootstrap/dist/css/bootstrap.min.css";
import NavBarr from '../templates/NavbarRecruiter';
import { responsiveFontSizes } from '@material-ui/core';
export default class CreateJob extends Component{
    constructor(props){
        super(props);
        this.state = {
            title: '',
            email: localStorage.getItem('id'),
            name : localStorage.getItem('name'),
            max_app: '',
            avl_pos: '',
            deadline: '',
            JobType: 'Full Time',
            duration: '',
            salary: '',
            skills: [''],
            applied: '0',
            filled: '0',
            status: 'active',
            date: ''
        }

        this.onChangeTitle = this.onChangeTitle.bind(this);
        this.onChangeMaxApp =this.onChangeMaxApp.bind(this);
        this.onChangeAvlJobs = this.onChangeAvlJobs.bind(this);
        this.onChangeDeadline = this.onChangeDeadline.bind(this);
        this.onChangeJobType = this.onChangeJobType.bind(this);
        this.onChangeSalary = this.onChangeSalary.bind(this);
        this.onChangeDuration = this.onChangeDuration.bind(this);
        this.addSkillField = this.addSkillField.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }
    fieldSkills(){
        return this.state.skills.map((ele, idx) => 
            idx !== 0 && <div>
                <input type="text" required placeholder="languages" value={this.state.skills[idx]} onChange={this.onChangeSkills.bind(this, idx)}/>
                <input type="button" value="x" onClick={this.removeSkill.bind(this, idx)}/>
            </div>
        )
    }
    addSkillField(){
        this.setState({ skills : [...this.state.skills,''] });
        this.fieldSkills();
    }
    onChangeTitle(event){
        this.setState({title : event.target.value});
    }
    onChangeMaxApp(event){
        this.setState({max_app : event.target.value});
    }
    onChangeAvlJobs(event){
        this.setState({avl_pos : event.target.value});
    }
    onChangeJobType(event){
        this.setState({JobType : event.target.value});
    }
    onChangeSalary(event){
        this.setState({salary : event.target.value});
    }
    onChangeDuration(event){
        this.setState({duration : event.target.value});
    }
    onChangeDeadline(event){
        this.setState({deadline : event.target.value});
        console.log(event.target.value);
    }
    onChangeSkills(idx, event){
        var temp = [...this.state.skills];
        temp[idx]=event.target.value;
        this.setState({ skills: temp })
    }
    removeSkill(idx){
        var temp = [...this.state.skills];
        temp.splice(idx,1);
        this.setState({ skills : temp });
        this.fieldSkills();
    }
    onSubmit(e){
        e.preventDefault();
        if(!this.state.title || !this.state.name || !this.state.email || !this.state.max_app || !this.state.avl_pos || !this.state.salary || !this.state.deadline || !this.state.JobType || this.state.skills.filter(ele => ele=='').length != 0 || this.state.duration > 6 || this.state.duration<=0  ){alert("Plz check all the fields")}
        else{
            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); 
            var yyyy = today.getFullYear();
            this.state.date = dd + "/" + mm + "/" + yyyy;
            axios.post('http://localhost:4000/createjob', this.state)
            .then(res => {
                if(res.data.msg === 'ok'){
                    alert('Posted a new job');
                    this.props.history.push("/Recruiter-Home");
                }
                else alert(res.data.msg);
            })
            .catch(err => alert(err));
        }
        console.log(this.state);
    }
    render() {
        return (
            <div>
                <NavBarr/>
                <br/>
                <form>
                    <div className="form-group">
                        <label><strong>CREATE A JOB </strong></label>
                    </div>
                    <div className="form-group">
                        <label>Job Title: </label>
                        <input type="text" 
                               className="form-control" 
                               value={this.state.title}
                               onChange={this.onChangeTitle}
                               required
                               />
                    </div>
                    <div className="form-group">
                        <label>Maximum number of applicants that can apply: </label>
                        <input type="number" 
                                min="1"
                               className="form-control" 
                               value={this.state.max_app }
                               onChange={this.onChangeMaxApp }
                               required
                               />
                    </div>
                    <div className="form-group">
                        <label>number of positions: </label>
                        <input type="number"
                               min="1" 
                               className="form-control" 
                               value={this.state.avl_pos }
                               onChange={this.onChangeAvlJobs }
                               required
                               />
                    </div>
                    <div className="form-group">
                        <label>Salary: </label>
                        <input type="number" 
                               className="form-control" 
                               value={this.state.salary }
                               onChange={this.onChangeSalary }
                               required
                               />
                    </div>
                    <div className="form-group">
                        <label>User Type: </label>
                        <br/>
                        <select  value={this.state.JobType} onChange={this.onChangeJobType.bind(this)}>
                            <option value="Full Time">Full Time</option>
                            <option value="Part Time">Part Time</option>
                            <option value="Work From Home">Work From Home</option>
                        </select>
                    </div>
                    <div className="form-group">
                        <label>Deadline: </label>
                        <input type="datetime-local" 
                               className="form-control" 
                               value={this.state.deadline }
                               onChange={this.onChangeDeadline }
                               required
                               />
                    </div>
                    <label>Skills </label>
                    <div className="form-group">
                        <input type="text" required placeholder="languages" value={this.state.skills[0]} onChange={this.onChangeSkills.bind(this, 0)}/>
                    </div>
                    {this.fieldSkills()}
                    <div className="form-group">
                        <input type="button"
                                value="+"
                                onClick={this.addSkillField }
                        />  
                    </div>
                    <div className="form-group">
                        <label>duration: </label>
                        <input type="number"
                                min="1" 
                                max="6"
                                placeholder="in months"
                               className="form-control" 
                               value={this.state.duration }
                               onChange={this.onChangeDuration }
                               required
                               />
                    </div>
                    <div className="form-group">
                        <input type="submit" value="Post" className="btn btn-primary" onClick={this.onSubmit}/>
                    </div>
                </form>
            </div>
        )
    }
}
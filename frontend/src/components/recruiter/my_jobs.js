import React, {Component} from 'react';
import axios from 'axios';
import "bootstrap/dist/css/bootstrap.min.css";
import NavBarr from '../templates/NavbarRecruiter';
import { responsiveFontSizes } from '@material-ui/core';
var temp
export default class MyJobs extends Component{
    constructor(props){
        super(props);
        this.state = {
            jobs: []
        }
    }
    componentDidMount(){
        axios.post('http://localhost:4000/createjob/viewjobs', {email:localStorage.getItem('id')})
            .then(res => {
                temp = [...res.data];
                console.log(temp);
                this.setState({
                    jobs: temp
                })
            })
            .catch(err => console.log(err));
    }
    onEdit(idx,e){
        e.preventDefault();
        localStorage.setItem('title', this.state.jobs[idx].title);
        this.props.history.push("/editjob");
    }
    onDelete(idx,e){
        e.preventDefault();
        axios.post('http://localhost:4000/createjob/deletejob', {title:this.state.jobs[idx].title, email:localStorage.getItem('id')})
            .then(res => {
                alert('deleted job ' + this.state.jobs[idx].title )
                this.props.history.push('/myjobs')
            })
            .catch(err => console.log(err));
        console.log(this.state.jobs[idx].title,localStorage.getItem('id'));
        axios.post("http://localhost:4000/newapplication/deletejob",{title:this.state.jobs[idx].title,email_recruiter :localStorage.getItem('id')})
            .then(res => this.props.history.push('/myjobs'))
            .catch(err => console.log(err));
        this.props.history.push('/');
    }
    onDetails(idx,e){
        localStorage.setItem('title', this.state.jobs[idx].title);
        this.props.history.push("/JobApplications");
    }
    render() {
        return (
            <div>
                <NavBarr/>
                <br/>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Job Title</th>
                            <th>Date of posting</th>
                            <th>No.of applications</th>
                            <th>positions left</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.jobs.map((ele,idx) => {
                                if(this.state.jobs[idx].status === 'active'){
                                return (
                                    <tr>
                                        <td><button type="button" class="btn btn-info" onClick={this.onDetails.bind(this,idx)}>{this.state.jobs[idx].title}</button></td>
                                        <td>{this.state.jobs[idx].date}</td>
                                        <td>{this.state.jobs[idx].applied}</td>
                                        <td>{Number(this.state.jobs[idx].positions) - Number(this.state.jobs[idx].filled)} </td>
                                        {console.log(this.state.jobs[idx].positions,this.state.jobs[idx].filled)}
                                        <td><input type="submit" value="delete" className="btn btn-danger" onClick={this.onDelete.bind(this,idx)}></input>&nbsp;/&nbsp;<input type="submit" value="edit" className="btn btn-primary" onClick={this.onEdit.bind(this,idx)}/></td>
                                    </tr>
                                )}
                            })
                        }
                    </tbody>
                </table>
            </div>
        )
    }
}
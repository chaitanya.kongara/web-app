import React, {Component} from 'react';
import axios from 'axios';
import "bootstrap/dist/css/bootstrap.min.css";
import NavBarr from '../templates/NavbarRecruiter';
var email,title;
var deadline,positions,applications,here;
export default class EditJob extends Component{
    constructor(props){
        super(props);
        this.state = {
            email:'',
            applications:'',
            positions:'',
            deadline:'',
            title:''
        }
    }
    componentDidMount(){
        var today = new Date()
        here = today.getFullYear() + '-' + String(today.getMonth() + 1).padStart(2, '0')  + '-' + today.getDate() +'T'+today.getHours() + ':' + today.getMinutes();
        email = localStorage.getItem('id');
        title = localStorage.getItem('title');
        this.setState({email:email, title:title})
        axios.post('http://localhost:4000/createjob/getjob',{email:email,title:title})
            .then(res => {
                this.setState({applications: res.data.applications,positions: res.data.positions, deadline:res.data.deadline})
                deadline = this.state.deadline;
                positions = this.state.positions;
                applications = this.state.applications;
            })
            .catch(err => alert(err));
    }
    onChangeApplications(event){
        this.setState({applications: event.target.value});
    }
    onChangeDeadline(event){
        this.setState({deadline:  event.target.value});
    }
    onChangePositions(event){
        this.setState({positions: event.target.value});
    }
    onSubmit(e){
        e.preventDefault();
        if(positions > this.state.positions || applications > this.state.applications || deadline > this.state.deadline) alert('You cant decrement these field');
        else{
        axios.post("http://localhost:4000/createjob/editjob", this.state)
            .then(res => {
                alert("Updated!");
                if(here < this.state.deadline && deadline <= this.state.deadline ){
                    axios.post('http://localhost:4000/createjob/statusactive',{email : localStorage.getItem('id'),title : localStorage.getItem('title')} )
                        .then(job => console.log('ok'))
                        .catch(err => console.log(err));
                }
                else if(here < this.state.deadline &&positions<this.state.positions){
                    axios.post('http://localhost:4000/createjob/statusactive',{email : localStorage.getItem('id'),title : localStorage.getItem('title')} )
                    .then(job => console.log('ok'))
                    .catch(err => console.log(err));
                }
                this.props.history.push('/myjobs');
            })
            .catch(err => alert(err))}
    }
    render(){
        return ( 
            <div>
                <NavBarr/>
                <form>
                    <div className="form-group">
                        <label>Max no.of applicants: </label>
                        <input type="number" 
                               className="form-control" 
                               value={this.state.applications}
                               onChange={this.onChangeApplications.bind(this)}
                               required
                               />
                    </div>
                    <div className="form-group">
                        <label>Max no.of positions: </label>
                        <input type="number" 
                               className="form-control" 
                               value={this.state.positions}
                               onChange={this.onChangePositions.bind(this)}
                               required
                               />
                    </div>
                    <div className="form-group">
                        <label>Deadline: </label>
                        <input type="datetime-local" 
                               className="form-control" 
                               value={this.state.deadline}
                               onChange={this.onChangeDeadline.bind(this)}
                               required
                               />
                    </div>
                    <div className="form-group">
                        <input type="submit" value="save" className="btn btn-primary" onClick={this.onSubmit.bind(this)}/>
                    </div>
                </form>
            </div>
        )
    }
}
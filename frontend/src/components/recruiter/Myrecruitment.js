import React, {Component} from 'react';
import axios from 'axios';
import "bootstrap/dist/css/bootstrap.min.css";
import NavBarr from '../templates/NavbarRecruiter';
export default class MyRecruitment extends Component{
    constructor(props){
        super(props);
        this.state = {
            applications: [],
            jobs: [],
            applicants: []
        }
    }
    componentDidMount(){
        axios.post("http://localhost:4000/newapplication/acceptedapplications", {email:localStorage.getItem('id')})
            .then(res => {
                this.setState({applications: res.data});
            })
            .catch(err => console.log(err));
        axios.get("http://localhost:4000/user/")
            .then(res => {
                this.setState({applicants: res.data});
                //console.log('hi' + res.data + this.state.applicants);
            })
            .catch(err => console.log(err));
        axios.post("http://localhost:4000/createjob/viewjobs",{email: localStorage.getItem('id')})
            .then(res => this.setState({jobs: res.data}))
            .catch(err => console.log(err));
    }   
    onReview(idx,e){
        e.preventDefault();
        var review = prompt('rating(on scale 0-5):');
        if(review != null){
            if(review == '' || review.length != 1 || (review[0] < '0' || review[0] > '5')) alert('enter on scale of 0-5, try again');
            else{
                axios.post("http://localhost:4000/user/review",{email: this.state.applications[idx].email_applicant, rating: review[0]})
                    .then( res => {
                        alert('rating sent')
                        axios.post("http://localhost:4000/newapplication/updatereview",{title:this.state.applications[idx].title,email_applicant:this.state.applications[idx].email_applicant, email_recruiter:this.state.applications[idx].email_recruiter})
                            .then(res => {
                                var temp = [...this.state.applications];
                                temp[idx].review = 'done';
                                this.setState({applications: temp});
                                console.log('processing');
                            })
                            .catch(err => console.log(err));
                    })
                    .catch(err => console.log(err));
            }
        }
    }    
    render(){
        return (
            <div>
                <NavBarr/>
                <br/>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Date of Joining</th>
                            <th>Job Type</th>
                            <th>Job Title</th>
                            <th>rating option</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.applications.map((ele, idx) => {
                                var name;
                                this.state.applicants.map((ele,ind)=>{
                                    if(this.state.applications[idx].email_applicant === this.state.applicants[ind].email){
                                        name = this.state.applicants[ind].fname + ' ' + this.state.applicants[ind].lname;
                                    }
                                })
                                var JobType;
                                this.state.jobs.map((ele,ind)=>{
                                    console.log(this.state.jobs[ind]);
                                    if(this.state.jobs[ind].email === this.state.applications[idx].email_recruiter && this.state.applications[idx].title === this.state.jobs[ind].title){
                                        JobType = this.state.jobs[ind].jobtype;
                                    }
                                })
                                return (
                                    <tr>
                                        <td>{name}</td>
                                        <td>{this.state.applications[idx].doj }</td>
                                        <td>{JobType}</td>
                                        <td>{this.state.applications[idx].title}</td>
                                        <td>{this.state.applications[idx].review === 'not yet done' && <button type="button" class="btn btn-primary" onClick={this.onReview.bind(this,idx)}>Review</button>}
                                        {this.state.applications[idx].review === 'done' && <a>Done</a>}</td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>
        )
    }
}
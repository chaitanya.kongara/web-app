import React, {Component} from 'react';
import axios from 'axios';
import "bootstrap/dist/css/bootstrap.min.css";
import Navbar from '../templates/Navbar'
var cnt = 0;
export default class Register extends Component {
    
    constructor(props) {
        super(props);

        this.state = {
            name: '',
            fname: '',
            lname: '',
            email: '',
            password: '',
            userType: '',
            education: [{Iname:'',Syear:'',Eyear:''}],
            skills: [''],
            number:null,
            bio:'',
            date:null
        }

        this.onChangeUsername = this.onChangeUsername.bind(this);
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
        this.onChangeuserType = this.onChangeuserType.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onChangefname = this.onChangefname.bind(this);
        this.onChangelname = this.onChangelname.bind(this);
        this.addField = this.addField.bind(this);
        this.addSkillField = this.addSkillField.bind(this);
        this.onChangenumber = this.onChangenumber.bind(this);
        this.onChangebio = this.onChangebio.bind(this);
    }

    fields(){
        return this.state.education.map((ele, idx) => 
         idx !== 0 && <div>
                <input type="text" required placeholder="Institution Name" value={this.state.education[idx].Iname} onChange={this.onChangeEducationI.bind(this, idx)}/>
                <input type="number" required placeholder="start year" min="1950" max="2021" step="1" value={this.state.education[idx].Syear} onChange={this.onChangeEducationS.bind(this, idx)}/>
                <input type="number" placeholder="end year" min={this.state.education[idx].Syear} max="2021" step="1" value={this.state.education[idx].Eyear} onChange={this.onChangeEducationE.bind(this, idx)}/>
                <input type="button" value="x" onClick={this.remove.bind(this, idx)}/>
            </div>
        )
    }
    fieldSkills(){
        return this.state.skills.map((ele, idx) => 
            idx !== 0 && <div>
                <input type="text" required placeholder="languages" value={this.state.skills[idx]} onChange={this.onChangeSkills.bind(this, idx)}/>
                <input type="button" value="x" onClick={this.removeSkill.bind(this, idx)}/>
            </div>
        )
    }
    onChangenumber(event){
        this.setState({number : event.target.value});
    }
    onChangeEducationI(idx, event){
        var temp = [...this.state.education];
        temp[idx].Iname=event.target.value;
        this.setState({education : temp})
    }
    onChangeEducationS(idx, event){
        var temp = [...this.state.education];
        temp[idx].Syear=event.target.value;
        this.setState({education : temp})
    }
    onChangeEducationE(idx, event){
        var temp = [...this.state.education];
        temp[idx].Eyear=event.target.value;
        this.setState({education : temp})
    }
    onChangeSkills(idx, event){
        var temp = [...this.state.skills];
        temp[idx]=event.target.value;
        this.setState({ skills: temp })
    }
    remove(idx){
        var temp = [...this.state.education];
        temp.splice(idx,1);
        this.setState({ education : temp });
        this.fields();
    }
    removeSkill(idx){
        var temp = [...this.state.skills];
        temp.splice(idx,1);
        this.setState({ skills : temp });
        this.fieldSkills();
    }
    onChangeUsername(event) {
        this.setState({ name: event.target.value });
    }
    onChangefname(event) {
        this.setState({ fname: event.target.value });
    }
    onChangelname(event) {
        this.setState({ lname: event.target.value });
    }
    onChangeEmail(event) {
        this.setState({ email: event.target.value });
    }
    onChangePassword(event) {
        this.setState({ password: event.target.value });
    }
    onChangeuserType(event) {
        this.setState({ userType: event.target.value });
        this.setState({education:[{Iname:'',Syear:'',Eyear:''}]});
        this.setState({ skills: [''] });
    }
    addField(){
        this.setState({ education : [...this.state.education,{Iname:'', Syear:'' , Eyear:''}] });
        this.fields();
    }
    addSkillField(){
        this.setState({ skills : [...this.state.skills,''] });
        this.fieldSkills();
    }
    onChangebio(event){
        cnt=0
        var str = event.target.value;
        str = str.trim();
        str = str.split(" ");
        str.map((ele,idx)=>{
            if(ele !== '') cnt++; 
        });
        if(cnt>250) alert('Oops! At max 250 words');
        else this.setState({bio: event.target.value});
    }   

    onSubmit(e) {
        e.preventDefault();
        if(this.state.userType === 'SELECT'){
            alert("Oops! Forgot to choose user-type");
        }else{
            if(this.state.userType === 'Job Applicant'){
                console.log('beep');
                const newUser = {
                    name: this.state.name,
                    email: this.state.email,
                    password: this.state.password,
                    userType: this.state.userType,
                    fname : this.state.fname,
                    lname: this.state.lname,
                    education: this.state.education,
                    skills: this.state.skills,
                    date: Date.now()
                }
                if(!this.state.name || !this.state.email || !this.state.password || !this.state.userType || !this.state.fname || !this.state.lname || this.state.education.filter(ele => (ele.Iname == '' || ele.Syear == '')&&(Number(ele.Syear) <=999 && Number(ele.Syear>9999))&&(ele.Eyear != ''&& Number(ele.Syear) <=999 && Number(ele.Syear)>9999) ).length != 0 || this.state.skills.filter(ele => ele == '' ).length != 0 ) alert('Plz check all the fields');
                else{
                    axios.post('http://localhost:4000/user/registerApplicant', newUser)
                    .then(res => {
                        alert("Created "+ newUser.userType + " " + newUser.name);
                        console.log(res.data);
                        localStorage.setItem("id",res.data.email);
                        localStorage.setItem("name",res.data.fname + ' ' +res.data.lname);
                        this.props.history.push('/Applicant-Home');
                    })
                    .catch(err => {alert("Oops! Email/Username already used");})
                    ;
                }
            }
            else{
                const newUser = {
                    name: this.state.name,
                    email: this.state.email,
                    password: this.state.password,
                    userType: this.state.userType,
                    bio : this.state.bio,
                    fname: this.state.fname,
                    lname: this.state.lname,
                    number: this.state.number,
                    date: Date.now()
                }
                if(!this.state.name || !this.state.email || !this.state.password || !this.state.userType || !this.state.bio || !this.state.fname || !this.state.lname || !this.state.number) {alert('Plz check all the fields')}
                else{
                    axios.post('http://localhost:4000/user/registerRecruiter', newUser)
                    .then(res => {
                        alert("Created "+ res.data.userType + " " + res.data.name);
                        console.log(res.data);
                        localStorage.setItem("id",res.data.email);
                        localStorage.setItem("name",res.data.fname + ' ' +res.data.lname);
                        this.props.history.push('/Recruiter-Home');
                    })
                    .catch(err => {alert("Oops! Email/Username already used");})
                    ;
                }
            }
        }
    }
    render() {
        return (
            <div>
                <Navbar/> 
                <br/>
                <form >
                    <div className="form-group">
                        <label>Username: </label>
                        <input type="text" 
                               className="form-control" 
                               value={this.state.name}
                               onChange={this.onChangeUsername}
                               required
                               />
                    </div>
                    <div className="form-group">
                        <label>Email: </label>
                        <input type="email" 
                               className="form-control" 
                               value={this.state.email}
                               onChange={this.onChangeEmail}
                               required
                               />  
                    </div>
                    <div className="form-group">
                        <label>Password: </label>
                        <input type="password" 
                               className="form-control" 
                               value={this.state.password}
                               onChange={this.onChangePassword}
                               required
                               />  
                    </div>
                    <div className="form-group">
                        <label>User Type: </label>
                        <br/>
                        <select  value={this.state.userType} onChange={this.onChangeuserType}>
                            <option value="SELECT">--SELECT--</option>
                            <option value="Job Applicant">Job Applicant</option>
                            <option value="Job Recruiter">Job Recruiter</option>
                        </select>
                    </div>
                    <br/>
                    {this.state.userType === 'Job Applicant' && <div className="form-group">
                        <label>First Name </label>
                        <input type="text" 
                               className="form-control" 
                               value={this.state.fname}
                               onChange={this.onChangefname}
                               required
                               />  
                    </div>
                    }
                    {this.state.userType === 'Job Applicant' && <div className="form-group">
                        <label>Last Name </label>
                        <input type="text" 
                               className="form-control" 
                               value={this.state.lname}
                               onChange={this.onChangelname}
                               required
                               />  
                    </div>
                    }
                    {console.log(this.state.education)}
                    {this.state.userType === 'Job Applicant' && <div className="form-group">
                        <label>Education </label> </div>}
                    {
                        this.state.userType === 'Job Applicant' && <div className="form-group">
                            <input type="text" required placeholder="Institution Name" value={this.state.education[0].Iname} onChange={this.onChangeEducationI.bind(this, 0)}/>
                            <input type="number" required placeholder="start year" min="1950" max="2021" step="1" value={this.state.education[0].Syear} onChange={this.onChangeEducationS.bind(this, 0)}/>
                            <input type="number" placeholder="end year" min={this.state.education[0].Syear} max="2021" step="1" value={this.state.education[0].Eyear} onChange={this.onChangeEducationE.bind(this, 0)}/>
                        </div>
                    }
                    {this.state.userType === 'Job Applicant' && this.fields()}
                    {this.state.userType === 'Job Applicant' && <div className="form-group">
                        <input type="button"
                                value="+"
                                onClick={this.addField}
                        />  
                    </div>
                    }
                    {console.log(this.state.skills)}
                    {this.state.userType === 'Job Applicant' && <div className="form-group">
                        <label>Skills </label> </div>}
                    {
                        this.state.userType === 'Job Applicant' && <div className="form-group">
                            <input type="text" required placeholder="languages" value={this.state.skills[0]} onChange={this.onChangeSkills.bind(this, 0)}/>
                        </div>
                    }
                    {this.state.userType === 'Job Applicant' && this.fieldSkills()}
                    {this.state.userType === 'Job Applicant' && <div className="form-group">
                        <input type="button"
                                value="+"
                                onClick={this.addSkillField}
                        />  
                    </div>
                    }
                    {this.state.userType === 'Job Recruiter' && <div className="form-group">
                        <label>First Name </label>
                        <input type="text" 
                               className="form-control" 
                               value={this.state.fname}
                               onChange={this.onChangefname}
                               required
                               />  
                    </div>
                    }
                    {this.state.userType === 'Job Recruiter' && <div className="form-group">
                        <label>Last Name </label>
                        <input type="text" 
                               className="form-control" 
                               value={this.state.lname}
                               onChange={this.onChangelname}
                               required
                               />  
                    </div>
                    }
                    {this.state.userType === 'Job Recruiter' && <div className="form-group">
                        <label>Contact Number </label>
                        <input type="number" min="1000000000" max="9999999999"
                               className="form-control" 
                               value={this.state.number}
                               onChange={this.onChangenumber}
                               required
                               />  
                    </div>
                    }
                    {this.state.userType === 'Job Recruiter' && <div className="form-group">
                        <label>Bio </label>
                        <input type="text"
                               placeholder="Bio(at max 250 words)"
                               className="form-control" 
                               value={this.state.bio}
                               onChange={this.onChangebio}
                               required
                               />  
                    </div>
                    }
                    <div className="form-group">
                        <input type="submit" value="Register" className="btn btn-primary" onClick={this.onSubmit}/>
                    </div>
                </form>
            </div>
        )
    }
}
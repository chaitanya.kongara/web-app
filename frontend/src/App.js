import React from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css"

import UsersList from './components/Users/UsersList';
import Home from './components/Common/Home';
import Register from './components/Common/Register';
import Login from './components/Common/Login';
import Profile from './components/Common/profile';
import CreateJob from './components/recruiter/createjob';
import RecruiterHome from "./components/recruiter/recruiter_home";
import ApplicantHome from './components/applicant/applicant_home';
import MyJobs from './components/recruiter/my_jobs';
import EditJob from './components/recruiter/edit_job';
import SearchJob from './components/applicant/search_jobs'
import MyApplications from './components/applicant/myapplications';
import JobApplications from './components/recruiter/Job_applications';
import MyRecruitment from './components/recruiter/Myrecruitment';
function App() {
  return (
    <Router>
      <div className="container">
        <Route path="/" exact component={Home}/>
        <Route path="/users" exact component={UsersList}/>
        <Route path="/register" exact component={Register}/>
        <Route path="/login" exact component={Login}/>
        <Route path="/profile" exact component={Profile}/>
        <Route path="/Applicant-Home" exact component={ApplicantHome}/>
        <Route path="/Recruiter-Home" exact component={RecruiterHome}/>
        <Route path="/createjob" exact component={CreateJob}/>
        <Route path="/myjobs" exact component={MyJobs}/>
        <Route path="/editjob" exact component={EditJob}/>
        <Route path="/search_job" exact component={SearchJob}/>
        <Route path="/myapplications" exact component={MyApplications}/>
        <Route path="/JobApplications" exact component={JobApplications}/>
        <Route path="/MyRecruitment" exact component={MyRecruitment}/>
      </div>
    </Router>
  );
}

export default App;
